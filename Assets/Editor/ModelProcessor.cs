﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System.Linq;

public class ModelProcessor : AssetPostprocessor
{
    public const string Material = "Assets/Dummy.mat";

    void OnPostprocessModel(GameObject g)
    {
        Debug.Log("WTF");

        var expectedMaterial = AssetDatabase.LoadAssetAtPath<Material>(Material);

        var checkMaterials = new Dictionary<string, Material>();
        checkMaterials.Add("Default-Material", expectedMaterial);

        var dirty = true;
        var renderers = g.GetComponentsInChildren<Renderer>(true);
        foreach (var renderer in renderers)
        {
            dirty = Replace(renderer, checkMaterials) || dirty;
        }

        if (dirty)
        {
            RemapDefaultMaterial(expectedMaterial);
        }
    }

    void RemapDefaultMaterial(Material expectedMaterial)
    {
        var modelImporter = assetImporter as ModelImporter;
        modelImporter.materialImportMode = ModelImporterMaterialImportMode.ImportViaMaterialDescription;
        modelImporter.materialLocation = ModelImporterMaterialLocation.InPrefab;
        modelImporter.materialName = ModelImporterMaterialName.BasedOnModelNameAndMaterialName;
        modelImporter.materialSearch = ModelImporterMaterialSearch.Local;

        var sourceMaterials = typeof(ModelImporter)
                .GetProperty("sourceMaterials", BindingFlags.NonPublic | BindingFlags.Instance)?
                .GetValue(modelImporter) as AssetImporter.SourceAssetIdentifier[];
        foreach (var identifier in sourceMaterials ?? Enumerable.Empty<AssetImporter.SourceAssetIdentifier>())
        {
            modelImporter.AddRemap(identifier, expectedMaterial);
        }

        modelImporter.SaveAndReimport();
    }

    public static bool Replace(Renderer renderer, Dictionary<string, Material> checkMaterial)
    {
        var materials = renderer.sharedMaterials;
        bool modified = false;

        for (int i = 0; i < materials.Length; ++i)
        {
            if (materials[i] == null)
                continue;

            var name = materials[i].name;

            Material mat;
            if (checkMaterial.TryGetValue(name, out mat))
            {
                materials[i] = mat;
                modified = true;
            }
        }

        if (modified)
        {
            renderer.sharedMaterials = materials;
        }

        return modified;
    }
}