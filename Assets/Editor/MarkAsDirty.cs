﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class test
{
    [MenuItem("Mark as dirty/test")]
    private static void NewMenuOption()
    {
        string path = "Assets/CUBE1.fbx";
        AssetDatabase.ImportAsset(path, ImportAssetOptions.ImportRecursive);

        GameObject fbx = (GameObject)AssetDatabase.LoadAssetAtPath(path, typeof(GameObject));
        
        //EditorUtility.SetDirty(fbx.GetComponent<Renderer>().material);
        Debug.Log("the fbx object and its material has been marked as dirty");
    }
}
